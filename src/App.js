import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import firebase from 'firebase';
import {NotificationContainer, NotificationManager} from 'react-notifications';


import Login from './pages/Login';
import Signup from './pages/Signup';

const config = {
    apiKey: 'AIzaSyC-GxGhrYrYjjM-AAXAWEUcohAQV-7KUK8',
    authDomain: 'fir-project-345d3.firebaseapp.com'
};
firebase.initializeApp(config);

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loggedIn: false,
            email:''
        };        
    }

    componentDidMount() {
        firebase.auth().onAuthStateChanged(user => {
            this.setState({ loggedIn: !!user });
            if(!! user) {
                NotificationManager.success('You have logged in successfully!');
                this.setState({email: user.email});                
            }
        });
    }

    render() {
        return (
            <div className="App">
                {this.state.loggedIn ? <div className="loginPage"><div className="wrapper signout">Logged in as  {this.state.email}!<br /> <button className="but" onClick={() => firebase.auth().signOut()}>Sign Out</button> </div></div>
                    :
                    <Router>
                        <Route path="/" exact={true} component={Login} />
                        <Route path="/signup" component={Signup} />
                    </Router>
                }
                <NotificationContainer />
            </div>
        );
    }
}

export default App;

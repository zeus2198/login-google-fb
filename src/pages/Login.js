import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import firebase from 'firebase';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import {NotificationManager} from 'react-notifications';

const uiConfig = {
    signInFlow: 'popup',
    signInOptions: [
        firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        firebase.auth.FacebookAuthProvider.PROVIDER_ID
    ],
    callbacks: {
        singInSuccess: () => false,
        processing: false
    }    
};

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email:'',
            pass:''
        };
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }   
    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }
    onSubmit(e) { 
        e.preventDefault();
        this.setState({processing: true});
        firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.pass)
        .catch(function(error) {
            NotificationManager.error(error.code);
        }).finally(() =>  this.setState({processing: false}));
    }
    render() {
        return <div className="loginPage">
                <div className="wrapper">
                    <img className="logo" src="./img/login.png" alt="Login" />
                    <form role="form" onSubmit={this.onSubmit}>
                        <input type="text" onChange={this.onChange} name="email" placeholder="Email" value={this.state.email} disabled={this.state.processing} required/> <br />
                        <input type="password" onChange={this.onChange} name="pass" placeholder="Password" value={this.state.pass} disabled={this.state.processing} required/>  <br />
                        <button type="submit" disabled={this.state.processing} className="but">{this.state.processing ? "Processing.." : "Sign In"}</button>  <br />
                        or  <br />
                        <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={firebase.auth()} /> 
                        ...or <Link to="/signup" disabled={this.state.processing}>Sign up</Link>
                    </form>
                </div>
            </div>
    }
}

export default Login;
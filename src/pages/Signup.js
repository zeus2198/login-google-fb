import React, { Component } from 'react';
import firebase from 'firebase';
import {NotificationManager} from 'react-notifications';

class Signup extends Component {
    constructor(props) {
        super(props);        
        this.state = {
            email:'',
            pass:'',
            passt:'',
            processing: false,
            props: props
        };
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }
    onSubmit(e) { 
        e.preventDefault();
        if(this.state.pass != this.state.passt) {
            NotificationManager.error('Password doesn\'t match!');
            return;
        }
        this.setState({processing: true});
        firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.pass).then(() => this.state.props.history.push("/"))
        .catch(function(error) {
            NotificationManager.error(error.code);
        }).finally(() =>  this.setState({processing: false}));
    }
    render() {
        return <div className="loginPage">            
                <div className="wrapper">
                    <img className="logo" src="./img/singup.png" alt="Login" />
                    <form role="form" onSubmit={this.onSubmit}>
                        <input type="email" onChange={this.onChange} name="email" placeholder="Email" value={this.state.email} disabled={this.state.processing} required/> <br />
                        <input type="password" onChange={this.onChange} name="pass" placeholder="Password" value={this.state.pass} disabled={this.state.processing} required/>  <br />
                        <input type="password" onChange={this.onChange} name="passt" placeholder="Password again" value={this.state.num} disabled={this.state.processing} required/>  <br />
                        <button type="submit" disabled={this.state.processing}  className="but">{this.state.processing ? "Processing.." : "Sign Up"}</button>  <br />                        
                    </form>
                </div>
        </div>
    }
}

export default Signup;